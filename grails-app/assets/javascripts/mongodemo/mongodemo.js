//= wrapped
//= require /angular/angular
//= require /mongodemo/core/mongodemo.core
//= require /mongodemo/index/mongodemo.index

angular.module("mongodemo", [
        "mongodemo.core",
        "mongodemo.index"
    ]);
